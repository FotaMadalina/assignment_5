import React,{Component} from 'react'

class RobotForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            name : '',
            type : '',
            mass: 0
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }

    }
    
    render(){
        return <div>
            <input type="text" placeholder="name"  onChange={this.handleChange} name="name"/>
            <input type="text" placeholder="type"  onChange={this.handleChange} name="type" />
            <input type="number" placeholder="mass" onChange={this.handleChange} name="mass" />
            <input type="button" value="add" onClick={() => {this.props.onAdd({
                name : this.state.name,
                type : this.state.type,
                mass : this.state.mass
            })}} />
        </div>
    }
}

export default RobotForm